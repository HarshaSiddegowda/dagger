package com.example.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pojo.Search;
import com.example.main.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    List<Search> myPojos;

    public RecyclerAdapter(List<Search> myPojos){
        this.myPojos=myPojos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.my_text_view, parent, false);
        return new MyViewHolder(listItem);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.tvTitleVal.setText(myPojos.get(position).getTitle());
        holder.tvYearVal.setText(myPojos.get(position).getYear());
        holder.tvMidVal.setText(myPojos.get(position).getImdbID());
        holder.tvTypeVal.setText(myPojos.get(position).getType());
        holder.tvPosterVal.setText(myPojos.get(position).getPoster());
    }

    @Override
    public int getItemCount() {
        return myPojos.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitleVal)
        TextView tvTitleVal;
        @BindView(R.id.tvYearVal)
        TextView tvYearVal;
        @BindView(R.id.tvMidVal)
        TextView tvMidVal;
        @BindView(R.id.tvTypeVal)
        TextView tvTypeVal;
        @BindView(R.id.tvPosterVal)
        TextView tvPosterVal;
        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
