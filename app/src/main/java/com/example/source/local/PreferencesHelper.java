package com.example.source.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.source.PreferenceHelperDataSource;
import com.google.gson.Gson;

import javax.inject.Inject;


public class PreferencesHelper implements PreferenceHelperDataSource {

    private String PREF_NAME = "getFlexyDriver";
    private SharedPreferences sharedPreferences=null;
    private Gson gson;
    private SharedPreferences.Editor editor=null;




    @Inject
    public PreferencesHelper(Context context) {
        int PRIVATE_MODE = 0;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
        editor.commit();
        gson = new Gson();
    }

}
