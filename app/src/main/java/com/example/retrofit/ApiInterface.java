package com.example.retrofit;

import com.example.pojo.PojoRes;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("?type=movie&apikey=5d81e1ce&page=1&s=guardians")
    Call<PojoRes> getAll();
}
