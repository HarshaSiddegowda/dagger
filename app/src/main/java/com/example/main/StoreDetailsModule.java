package com.example.main;

import android.app.Activity;

import com.example.dragger.ActivityScoped;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class StoreDetailsModule
{
    @ActivityScoped
    @Binds
    abstract Activity getActivity(MainActivity activity);

    @Binds
    @ActivityScoped
    abstract MainViewPresenter.view getView(MainActivity view);

    @Binds
    @ActivityScoped
    abstract MainViewPresenter.presenter getPresenter(Presenter presenter);
}
