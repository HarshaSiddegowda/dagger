package com.example.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.example.pojo.PojoRes;
import com.example.pojo.Search;
import com.example.retrofit.ApiInterface;
import com.example.sqlLite.ResultDataBase;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Presenter implements MainViewPresenter.presenter {
    @Inject
    ApiInterface apiInterface;
    @Inject MainViewPresenter.view view;
    @Inject Context context;

    /*final SharedPreferences sharedPreferences;*/
    SharedPreferences.Editor editor;
    public static final String TAG="check";

    private ResultDataBase resultDataBase;

    @Inject
    Presenter(){

       /* sharedPreferences = PreferenceManager.getDefaultSharedPreferences((Context) view);*/

    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void intialize(boolean isLoading) {
        resultDataBase = ResultDataBase.getInstance(context);

        view.showProgess();
        /* editor = sharedPreferences.edit();*/
        Call<PojoRes> call = apiInterface.getAll();

        call.enqueue(new Callback<PojoRes>() {
            @Override
            public void onResponse(@NotNull Call<PojoRes> call, @NotNull Response<PojoRes> response) {
                view.hideProgess();
                if (response.code() == 200) {
                    PojoRes pojoRes= response.body();
                    if (pojoRes != null) {
                        Log.d(TAG, "reponse body: "+pojoRes.getSearch().size());
                        Log.d(TAG, "reponse body: "+pojoRes.getSearch().get(0).getYear());
                    }
                    assert pojoRes != null;
                    saveData(pojoRes,isLoading);

                }
            }

            @Override
            public void onFailure(@NotNull Call<PojoRes> call, @NotNull Throwable t) {
                view.hideProgess();
                Log.d(TAG, "onFailure: "+call.toString());
                Log.d(TAG, "onFailure: Throwable "+t.getMessage());
            }
        });
    }

    @Override
    public void getData() {
        List<Search> res=resultDataBase.getResultDeo().getPersonList();
        Log.d("exe", "getFname: "+res.size());
        view.setData(res);
    }

    private void saveData(PojoRes pojoRes,boolean isloading) {
        if(!isloading)
        resultDataBase.getResultDeo().deteDataBase();

        for (int i = 0; i < pojoRes.getSearch().size(); i++) {
            resultDataBase.getResultDeo().insertPerson(pojoRes.getSearch().get(i));
            Log.d("exe", "getFname: myPojo " + i + " " + pojoRes.getSearch().get(i).getTitle());

        }
        getData();

    }
}
