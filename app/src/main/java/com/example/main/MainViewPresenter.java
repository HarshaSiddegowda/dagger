package com.example.main;

import com.example.pojo.Search;

import java.util.List;

public interface MainViewPresenter  {
    interface view{
        /**
         * set the value to recyclerview
         * @param response: stored data
         */
        void setData(List<Search> response);

        /**
         * show progressbar
         */
        void showProgess();

        /**
         * hide progressBar
         */
        void hideProgess();
    }
    interface presenter{
        /**
         * intialize or calling the Api
         */
        void intialize(boolean isLoading);

        /**
         * get the data from database
         */
        void getData();

    }
}
