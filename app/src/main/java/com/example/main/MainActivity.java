package com.example.main;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.adapter.RecyclerAdapter;
import com.example.pojo.Search;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements MainViewPresenter.view{

    @Inject MainViewPresenter.presenter presenter;
    RecyclerView rview;
    RecyclerAdapter recyclerAdapter;
    List<Search> resArr=new ArrayList<>();
    private static final String LOG_TAG = "CheckNetworkStatus";
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    private TextView networkStatus;
    public static final String TAG="check";
    Animation a;
    boolean isLoading = false;

    @BindView(R.id.progressBar)
            ProgressBar progressBar;
    @BindView(R.id.search)
            EditText search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        search.addTextChangedListener(textWatcher);
        setViews();
    }

    /**
     *view intialization
     */
    private  void setViews(){
        rview=findViewById(R.id.adapter);
        recyclerAdapter=new RecyclerAdapter( resArr);                                //adding the recycler view
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rview.setLayoutManager(layoutManager);
        rview.setAdapter(recyclerAdapter);
        recyclerAdapter.notifyDataSetChanged();

        presenter.intialize(false);                                                      //calling API
        presenter.getData();                                                        //retriving the data from databse
        networkStatus =  findViewById(R.id.networkStatus);

        a = AnimationUtils.loadAnimation(this, R.anim.animation);           //giving animation for Network manager text
        a.reset();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);

        rview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == resArr.size() -1) {
                        //bottom of list!
                        presenter.intialize(true);
                        isLoading = true;
                    }
                }
            }
        });
    }

    /**
     * Retriving the data from database and adding in recyclerview
     * @param response store data
     */
    @Override
    public void setData(List<Search> response)  {
        try {
            isLoading=false;
            resArr.clear();
            resArr.addAll(response);
            Log.d(TAG, "setData: "+resArr.size());
            recyclerAdapter.notifyDataSetChanged();
        }catch (Exception e){
            Log.d(TAG, "setData: exception "+e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * visiabling the progressbar
     */
    @Override
    public void showProgess() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * hiding the progressbar
     */
    @Override
    public void hideProgess() {
        progressBar.setVisibility(View.GONE);
    }

    /**
     * textchange listner for Edittext
     */
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length()>2){
                presenter.intialize(false);                                         //callimg API
                Log.d(TAG, "afterTextChanged: if"+s);
            }
            else {
                Log.d(TAG, "afterTextChanged: else"+s);
            }

        }


    };


    @Override
    protected void onDestroy() {
        Log.v(LOG_TAG, "onDestory");
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    /**
     * using the BroadcastReceiver to check internet connection
     */
    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            Log.v(LOG_TAG, "Receieved notification about network status");
            isNetworkAvailable(context);

        }

        /**
         * checking the internet connection
         * @param context passing acivity
         */
        private void isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                for (NetworkInfo networkInfo : info) {
                    if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                        if (!isConnected) {
                            Log.v(LOG_TAG, getResources().getString(R.string.not_connected));
                            networkStatus.setText(getResources().getString(R.string.not_connected));
                            networkStatus.setVisibility(View.GONE);
                            isConnected = true;
                            presenter.intialize(false);
                        }
                        return;
                    }
                }
            }

            Log.v(LOG_TAG, getResources().getString(R.string.not_connected));
            networkStatus.setText(getResources().getString(R.string.not_connected));
            networkStatus.setVisibility(View.VISIBLE);
            networkStatus.clearAnimation();
            networkStatus.startAnimation(a);
//            overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
            isConnected = false;
        }
    }
}
