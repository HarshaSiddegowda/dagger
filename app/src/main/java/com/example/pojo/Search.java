package com.example.pojo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "search")
public class Search implements Serializable {

    @ColumnInfo(name="type")
    private String Type;

    @ColumnInfo(name="year")
    private String Year;

    @ColumnInfo(name="imobId")
    private String imdbID;

    @ColumnInfo(name="poster")
    private String Poster;

    @PrimaryKey(autoGenerate = true)
    private int pid;

    @ColumnInfo(name="title")
    private String Title;

    public String getType ()
    {
        return Type;
    }

    public void setType (String Type)
    {
        this.Type = Type;
    }

    public String getYear ()
    {
        return Year;
    }

    public void setYear (String Year)
    {
        this.Year = Year;
    }

    public String getImdbID ()
    {
        return imdbID;
    }

    public void setImdbID (String imdbID)
    {
        this.imdbID = imdbID;
    }

    public String getPoster ()
    {
        return Poster;
    }

    public void setPoster (String Poster)
    {
        this.Poster = Poster;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Type = "+Type+", Year = "+Year+", imdbID = "+imdbID+", Poster = "+Poster+", Title = "+Title+"]";
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }
}
