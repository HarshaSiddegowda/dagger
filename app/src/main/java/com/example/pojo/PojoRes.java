package com.example.pojo;

import java.io.Serializable;
import java.util.ArrayList;

public class PojoRes implements Serializable {
    private String Response;

    private String totalResults;

    private ArrayList<Search> Search;

    public String getResponse ()
    {
        return Response;
    }

    public void setResponse (String Response)
    {
        this.Response = Response;
    }

    public String getTotalResults ()
    {
        return totalResults;
    }

    public void setTotalResults (String totalResults)
    {
        this.totalResults = totalResults;
    }

    public ArrayList<Search> getSearch ()
    {
        return Search;
    }

    public void setSearch (ArrayList<Search> Search)
    {
        this.Search = Search;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Response = "+Response+", totalResults = "+totalResults+", Search = "+Search+"]";
    }
}
