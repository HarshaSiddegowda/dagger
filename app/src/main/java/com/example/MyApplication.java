package com.example;

import android.accounts.AccountManager;
import android.content.Context;

import com.example.dragger.AppComponent;
import com.example.dragger.DaggerAppComponent;
import com.example.source.PreferenceHelperDataSource;
import javax.inject.Inject;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;


public class MyApplication extends DaggerApplication {

    @Inject
    PreferenceHelperDataSource preferenceHelperDataSource;

    @Override
    public void onLowMemory() {
        System.gc();
        super.onLowMemory();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }


    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


}

