package com.example.sqlLite;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.pojo.Search;

import java.util.List;

@Dao
public interface ResultDeo {

    @Query("Select * from search")
    List<Search> getPersonList();

    @Insert
    void insertPerson(Search search);

    @Query("DELETE from search")
    void deteDataBase();
}
