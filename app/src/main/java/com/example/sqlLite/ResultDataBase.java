package com.example.sqlLite;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.pojo.Search;


@Database(entities = Search.class,exportSchema = false,version = 1)
public abstract class ResultDataBase extends RoomDatabase {

    private static final String DB_NAME="person_db";

    public abstract ResultDeo getResultDeo();

    private static ResultDataBase resultDataBase;

    public static ResultDataBase getInstance(Context context) {
        if (null == resultDataBase) {
            resultDataBase = buildDatabaseInstance(context);
        }
        return resultDataBase;
    }

    private static ResultDataBase buildDatabaseInstance(Context context) {
        return Room.databaseBuilder(context,
                ResultDataBase.class,
                DB_NAME)
                .allowMainThreadQueries().build();
    }
   /* private static final String DB_NAME="person_db";
    private static ResultDataBase instance;
    public static synchronized ResultDataBase getInstance(Context context){
        if(instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(),ResultDataBase.class,DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract ResultDeo resultDeo();*/
}
