package com.example;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


import com.example.source.PreferenceHelperDataSource;
import com.example.main.MainActivity;
import com.example.main.R;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class SplashScreen extends DaggerAppCompatActivity {

    @Inject
    PreferenceHelperDataSource preferenceHelperDataSource;
    private Runnable runnable;
    private Handler handler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_scrren);
        runnable=new Runnable() {


            @Override
            public void run() {
                checkConfiguration();

            }
        };
        handler=new Handler();
        handler.postDelayed(runnable, 2000);
    }

    /**
     * <h2>checkConfiguration</h2>
     * <p>check user is logged in or not and
     * based on status it corresponding activity will open</p>
     */
    private void checkConfiguration() {
        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}
